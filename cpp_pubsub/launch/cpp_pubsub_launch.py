from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='cpp_pubsub',
            node_executable='talker',
            name='talker',
            output='screen',
        ),
        Node(
            package='cpp_pubsub',
            node_executable='listener',
            name='listener',
            output='screen',
        ),
    ])
